const fs = require('fs');
const os = require('os');
const path = require('path');
const slueStream = require('slue-stream');
const vueCompiler = require('vue-template-compiler');
const styleCompiler = require('./lib/styleCompiler');
const templateCompiler = require('./lib/templateCompiler');
const scriptCompiler = require('./lib/scriptCompiler');
const sourceMap = require('source-map');

const vueScriptStart = `${os.EOL}/*** vue-script-start ***/${os.EOL}`;
const vueScriptEnd = `${os.EOL}/*** vue-script-end ***/${os.EOL}`;

const insertCss = fs.readFileSync(path.resolve(__dirname, './lib/insert-css.js'), 'utf8');

function createConsumer(res) {
    return new Promise(function(resolve) {
        if (res.map && res.code) {
            sourceMap.SourceMapConsumer.with(res.map, null, function(consumer) {
                res.consumer = consumer;
                resolve(res);
            });
        } else {
            resolve(res);
        }
    });
}

module.exports = function(options = {}) {
    if (typeof options.sourceMap !== 'boolean') {
        options.sourceMap = false;
    }

    return slueStream.transformObj(function(file, env, cb) {
        let extname = path.extname(file.path);
        if (extname === '.vue') {
            let contents = file.contents.toString();
            let handledContents = vueCompiler.parseComponent(contents, {
                pad: true
            });

            let _node = new sourceMap.SourceNode(null, null, null, null);
            options.path = file.path;
            scriptCompiler(handledContents, options).then(function(res) {
                return createConsumer(res);
            }).then(function(res) {
                if (res.consumer) {
                    _node = sourceMap.SourceNode.fromStringWithSourceMap(res.code, res.consumer);
                } else if (res.code) {
                    _node.add(res.code);
                }
                if (res.code) {
                    _node.prepend(`(function () {${vueScriptStart}`);
                    _node.add(`${vueScriptEnd}})()${os.EOL}`);
                    _node.add(`if (module.exports.__esModule) { module.exports = module.exports.default; }${os.EOL}`);
                }
                return styleCompiler(handledContents, file.path);
            }).then(function(res) {
                let opts = {};
                if (res) {
                    _node.add(insertCss);
                    _node.add(`insert("${res.code}");${os.EOL}`);
                    if (res.id) {
                        opts.id = res.id;
                    }
                }

                return templateCompiler(handledContents, opts);
            }).then(function(res) {
                if (res) {
                    _node.add(`module.exports.render = ${res.render};${os.EOL}`);
                    _node.add(`module.exports.staticRenderFns = ${res.staticRenderFns};${os.EOL}`);
                }

                let generator = _node.toStringWithSourceMap();
                file.sourceMap = generator.map.toString();
                file.contents = Buffer.from(_node.toString().replace(/;(undefined)+/g, ''));

                cb(null, file);
            });
        } else {
            cb(null, file);
        }
    });
}