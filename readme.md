# slue-vue-compiler

The slue plugin to compile vue。

```javascript
const slue = require('slue');
const slueVueCpmpiler = require('slue-vue-compiler');

slue.read('./test/app.vue')
    .pipe(slueVueCpmpiler())
    .pipe(slue.write('./test/dist/', {
        ext: '.js',
        filename: 'app'
    }));
```