const md5 = require('md5');
const vueCompiler = require('vue-template-compiler');
const selectorParser = require('postcss-selector-parser')
const postcss = require('postcss');
const less = require('less');

let currentId = '';
var addId = postcss.plugin('add-id', function() {
    return function(root) {
        root.each(function rewriteSelector(node) {
            if (!node.selector) {
                // handle media queries
                if (node.type === 'atrule' && node.name === 'media') {
                    node.each(rewriteSelector);
                }
                return
            }

            var transform = function(selectors) {
                selectors.each(function(selector) {
                    var node = null
                    selector.each(function(n) {
                        if (n.type !== 'pseudo') node = n
                    })
                    selector.insertAfter(node, selectorParser.attribute({
                        attribute: currentId
                    }))
                })
            }

            node.selector = selectorParser(transform).processSync(node.selector);
        })
    }
});

process.on('unhandledRejection', function() {});

function compileLess(styles, path) {
    let promise = new Promise(function(resolve, reject) {
        resolve();
    });
    styles.forEach(function(item) {
        promise = promise.then(function() {
            return less.render(item.content, {
                compress: false,
                paths: []
            }).then(function(res) {
                if (res.css) {
                    item.content = res.css
                }
            }, function(err) {
                err.filePath = path;
                console.log(err);
            });
        });
    });
    return promise;
}

module.exports = function(handledContents, path) {
    return new Promise(function(resolve, reject) {
        if (handledContents.styles && handledContents.styles.length) {
            compileLess(handledContents.styles, path).then(function() {
                currentId = 'data-v-' + md5(path).slice(0, 8);
                let allStyle = [];
                let scopedStyle = '';
                handledContents.styles.forEach(function(item) {
                    if (item.scoped == true) {
                        scopedStyle = scopedStyle + item.content;
                    } else {
                        allStyle.push(item.content);
                    }
                });

                if (scopedStyle) {
                    postcss([addId]).process(scopedStyle, {
                        from: '',
                        to: ''
                    }).then(function(result) {
                        let code = allStyle.join('') + result.css;
                        code = code.replace(/\n|\t|\r/g, '');
                        resolve({
                            id: currentId,
                            code: code
                        });
                    });
                } else {
                    let code = allStyle.join('');
                    code = code.replace(/\n|\t|\r/g, '');
                    resolve({
                        code: code
                    });
                }
            });
        } else {
            resolve();
        }
    });
}