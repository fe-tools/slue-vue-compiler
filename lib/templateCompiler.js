const parse5 = require('parse5');
var vueCompiler = require('vue-template-compiler')
var transpile = require('vue-template-es2015-compiler')

function addId(node, id) {
    if (node.childNodes) {
        node.childNodes.forEach(function(subNode) {
            if (subNode.tagName != 'html' && subNode.tagName != 'head' && subNode.tagName != 'body') {
                if (subNode.attrs) {
                    subNode.attrs.push({
                        name: id,
                        value: ''
                    });
                }
            }
            if (subNode.childNodes) {
                addId(subNode, id);
            }
        });
    }
}

function toFunction(code) {
    return transpile('function render () {' + code + '}')
}

module.exports = function(handledContents, opts) {
    return new Promise(function(resolve, reject) {
        if (handledContents.template) {
            var template = handledContents.template;
            if (opts.id) {
                var document = parse5.parse(template.content);
                addId(document, opts.id);
                handledContents.template.content = parse5.serialize(document).replace(/^<html><head><\/head><body>/, '').replace(/<\/body><\/html>$/, '');
            }
            var compiled;
            try {
                compiled = vueCompiler.compile(template.content);
            } catch (e) {
                console.log(e);
            }
            resolve({
                render: toFunction(compiled.render),
                staticRenderFns: '[' + compiled.staticRenderFns.map(toFunction).join(',') + ']'
            });
        } else {
            resolve();
        }
    });
}