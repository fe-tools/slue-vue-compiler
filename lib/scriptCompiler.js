const os = require('os');
const path = require('path');
const babel = require('babel-core');
const sourceMap = require('source-map');

function isNumber(v) {
    return typeof v === 'number';
}

module.exports = function(handledContents, opts) {
    return new Promise(function(resolve, reject) {
        if (handledContents.script) {
            try {
                let content = handledContents.script.content;
                content = handledContents.script.content.replace(/((\r?\n)?\/\/(\r?\n)+)+/g, '');

                let scriptContent;
                try {
                    scriptContent = babel.transform(content, {
                        compact: false,
                        sourceMaps: opts.sourceMap,
                        presets: [
                            require('babel-preset-env'),
                            require('babel-preset-stage-3')
                        ]
                    });
                } catch (e) {
                    e.filePath = opts.path;
                    console.log(e);
                    process.exit(1);
                }

                if (scriptContent.map) {
                    let pcwd = path.resolve(process.cwd(), '../');
                    let relativePath = path.relative(pcwd, opts.path);
                    let moduleId = relativePath.replace(/\\/g, '/');
                    scriptContent.map.sources = [`sluepack:///${moduleId}`];

                    if (handledContents.template && handledContents.template.content) {
                        let templateContent = handledContents.template.content.split(os.EOL);
                        templateContent = templateContent.filter(function(line) {
                            return line != '';
                        });
                        templateContent.unshift('<template>');
                        templateContent.push('</template>');

                        sourceMap.SourceMapConsumer.with(scriptContent.map, null, function(consumer) {
                            const generator = new sourceMap.SourceMapGenerator({
                                file: path.basename(opts.path),
                                sourceRoot: `sluepack:///${moduleId}`
                            });
                            consumer.eachMapping(function(m) {
                                if (
                                    isNumber(m.generatedLine) &&
                                    isNumber(m.generatedColumn) &&
                                    isNumber(m.originalLine) &&
                                    isNumber(m.originalColumn)
                                ) {
                                    generator.addMapping({
                                        source: m.source,
                                        original: {
                                            line: m.originalLine + templateContent.length + 1,
                                            column: m.originalColumn
                                        },
                                        generated: {
                                            line: m.generatedLine,
                                            column: m.generatedColumn
                                        }
                                    });
                                }
                            });

                            const sourcesContent = `${templateContent.join(os.EOL)}${os.EOL}<script>${os.EOL}${scriptContent.map.sourcesContent[0]}</script>${os.EOL}`;
                            generator.setSourceContent(`sluepack:///${moduleId}`, sourcesContent);
                            scriptContent.map = JSON.parse(generator.toString());

                            resolve(scriptContent);
                        });
                    }

                } else {
                    resolve(scriptContent);
                }
            } catch (e) {
                reject(e);
            }
        } else {
            resolve();
        }
    });
}