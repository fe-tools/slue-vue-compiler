const path = require('path');
const os = require('os');
const sourceMap = require('source-map');
const vueScriptStart = `/*** vue-script-start ***/`;

process.on('unhandledRejection', function() {});

module.exports = function(scriptContent, opts) {
    return new Promise(function(resolve, reject) {
        if (scriptContent.map) {
            let _sourcemap = new sourceMap.SourceMapGenerator();
            let compiledRes = opts.compiledRes.join(os.EOL);
            compiledRes = compiledRes.split(/\r?\n/);
            let theStartLine = compiledRes.indexOf(vueScriptStart);
            let sourcePath = path.relative(process.cwd(), opts.file.path);
            sourcePath = sourcePath.replace(/\\/g, '/');
            sourcePath = `sluepack:///${sourcePath}`;
            let whatever = sourceMap.SourceMapConsumer.with(scriptContent.map, null, function(consumer) {
                consumer.eachMapping(function(mapping) {
                    if (mapping.generatedLine 
                        && mapping.generatedColumn 
                        && mapping.originalLine
                        && mapping.originalColumn
                    ) {
                        _sourcemap.addMapping({
                            generated: {
                                line: mapping.generatedLine + theStartLine - 1,
                                column: mapping.generatedColumn
                            },
                            source: sourcePath,
                            original: {
                                line: mapping.originalLine,
                                column: mapping.originalColumn
                            }
                        });
                    }
                });
                _sourcemap.setSourceContent(sourcePath, opts.file.contents.toString());
                resolve({
                    sourceMap: _sourcemap.toString()
                })
            });
        } else {
            resolve();
        }
    });
}