# slue-vue-compiler changelog

## 1.1.6

### less编译错误提示

## 1.2.0

### support sourcemap

## 1.2.1

### support sourcemap

## 1.2.2

### sourcemap bugfix.

## 1.2.3

### template-compiler bugfix.

## 1.2.4

### remove '//' lines.

## 1.2.5

### bugfix.

## 1.2.6

### bugfix

## 1.2.7

### sourcemap path add project folder

## 1.2.8

### console error when compile failed.

## 1.2.9

### babel transform 500k.