const slue = require('slue');
const slueVueCpmpiler = require('../index');

slue.read('./test/app.vue')
    .pipe(slueVueCpmpiler())
    .pipe(slue.write('./test/dist/', {
        ext: '.js',
        filename: 'tts'
    }));